<?php
return [
    'siteName'          => 'Emma De Marco',

    //'ownerName'         => 'Ross Bischoff',
    //'color'             => '#144EA1',

    'ownerName'         => 'Emma De Marco',
    'color'             => '#ce302b',

    // This is the only Agent's properties that will show
    'filterAgentEmail' => 'emma@remax.com.au',

    'teamEmails' => ['emma@remax.com.au', 'rbischoff@remax.com.au'],

    // This is the from email address your emails with appear to come from
    'fromEmail' => 'emma@remax.com.au',

    'renetImportFolder' => '/var/www/emmaremax/import',

    // Change this to something, anything you want
    'secret' => 'oeohjgkjdfghe924wgkjgd239ed',

    // This is what will be displayed on the home page
    'contactEmail' => 'emma@remax.com.au',
    //'contactEmail' => 'rbischoff@remax.com.au',

    'cacheKeys' => [

    ],
];
