<?php
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */

/* @var $msg String */
/* @var $fromName String */
/* @var $fromEmail String */
/* @var $fromPhone String */
/* @var $property \common\models\Property */

?>

WEB FORM CONTACT
<?= $property->getStreetAddress() ?>
======================

<?= $fromName ?>
<?= $fromEmail ?>
<?= $fromPhone ?>


<?= $msg ?>

======================

Sent on <?= date('jS F Y, g:ia') ?> via <?= $_SERVER['HTTP_HOST'] ?>
IP <?= $_SERVER['REMOTE_ADDR'] ?>
