<?php

namespace common\models\xml;

use common\components\Util;
use Sabre\Xml\Reader as XmlReader;


class RenetProperties {

    /**
     * @var bool
     */
    public $isLoaded = false;

    /**
     * @var int
     */
    public $timestamp = 0;

    /**
     * @var BasicObject[]
     */
    public $residentials = [];


    /**
     * Constructor
     *
     * @access public
     * @param  string  $xmlString
     */
    public function __construct(&$xmlString) {
        $this->parse($xmlString);
    }


    /**
     * parse
     *
     * @param  string  $xmlString
     * @return bool
     * @throws \Exception
     */
    protected function parse(&$xmlString) {
        $reader = new XmlReader();

        // Basic Function for items with unique keys
        $basicFunction = function($reader) {
            /* @var $reader XmlReader */
            $object   = new BasicObject;
            $children = $reader->parseInnerTree();
            if ($children) {
                foreach ($children as $child) {
                    $object->set($child['name'], $child['value']);
                    $object->setAttribute($child['name'], $child['attributes']);
                }
            }
            return $object;
        };

        // Function specifically for the 'options' key
        $objectsFunction = function($reader) {
            /* @var $reader XmlReader */
            $children = $reader->parseInnerTree();
            $objects  = [];
            if ($children) {
                foreach ($children as $child) {
                    $key = Util::cleanXmlKey($child['name']);
                    // Make sure image url is supplied, cause renet are stupid
                    if ($key !== 'img' || isset($child['attributes']['url']) && $child['attributes']['url']) {
                        $object = new BasicObject;
                        // We want the attributes to be the data in this case
                        $object->setData($child['attributes']);
                        $object->set('type', $key);
                        $objects[] = $object;
                    }
                }
            }
            return $objects;
        };

        // Function to process items without a unique key
        $arrayFunction = function($reader) {
            /* @var $reader XmlReader */
            $children = $reader->parseInnerTree();
            $objects  = [];
            if ($children) {
                foreach ($children as $child) {
                    $key = Util::cleanXmlKey($child['name']);
                    // Make sure image url is supplied, cause renet are stupid
                    $object = new BasicObject;
                    // We want the attributes to be the data in this case

                    $object->setAttributes($child['attributes']);
                    $object->set('name', $key);
                    $object->set('value', $child['value']);
                    $objects[] = $object;
                }
            }
            return $objects;
        };

        // Mapping of functions
        $reader->elementMap = [
            '{}propertyList' => function($reader) {
                /* @var $reader XmlReader */
                $properties = new BasicObject;
                $properties->setAttributes($reader->parseAttributes());
                $children = $reader->parseInnerTree();
                foreach ($children as $child) {
                    if ($child['value'] instanceof BasicObject) {
                        $properties->residentials[] = $child['value'];
                    }
                }
                return $properties;
            },
            '{}residential' => function($reader) {
                /* @var $reader XmlReader */
                $residential = new Property;
                $children    = $reader->parseInnerTree();

                $listingAgents = [];

                foreach ($children as $child) {
                    if ($child['name'] == '{}listingAgent') {
                        $listingAgents[] = $child['value'];
                    } else {
                        $residential->set($child['name'], $child['value']);
                        $residential->setAttribute($child['name'], $child['attributes']);
                    }
                }

                if (count($listingAgents)) {
                    $residential->set('{}listingAgent', $listingAgents);
                }

                $residential->set('propertyType', 'residential');
                return $residential;
            },
            '{}land' => function($reader) {
                /* @var $reader XmlReader */
                $land = new Property;
                $children    = $reader->parseInnerTree();
                foreach ($children as $child) {
                    $land->set($child['name'], $child['value']);
                    $land->setAttribute($child['name'], $child['attributes']);
                }
                $land->set('propertyType', 'land');
                return $land;
            },
            '{}rental' => function($reader) {
                /* @var $reader XmlReader */
                $rental = new Property;
                $children    = $reader->parseInnerTree();
                foreach ($children as $child) {
                    $rental->set($child['name'], $child['value']);
                    $rental->setAttribute($child['name'], $child['attributes']);
                }
                $rental->set('propertyType', 'rental');
                return $rental;
            },
            '{}objects'         => $objectsFunction,
            '{}listingAgent'    => $arrayFunction,
            '{}address'         => $basicFunction,
            '{}features'        => $basicFunction,
            '{}soldDetails'     => $basicFunction,
            '{}landDetails'     => $basicFunction,
            '{}buildingDetails' => $basicFunction,
            '{}inspectionTimes' => $arrayFunction,
        ];

        // Get parsing!
        $reader->xml($xmlString);
        $result = $reader->parse();

        if ($result && isset($result['value'])) {

            // Simplify:
            $result = $result['value'];

            /* @var $result BasicObject */
            $this->timestamp = self::parseDate($result->getAttribute('date'));
            if (!$this->timestamp) {
                throw new \Exception('Could not determine timestamp from XML');
            }

            if (isset($result->residentials)) {
                $this->residentials = $result->residentials;
            }

            $this->isLoaded = true;
            return true;
        }

        return false;
    }


    /**
     * @param  $dateString
     * @return int
     */
    public static function parseDate($dateString)
    {
        if (preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2})-([0-9]{2}):([0-9]{2}):([0-9]{2})/uis", $dateString, $matches)) {
            return mktime($matches[4], $matches[5], $matches[6], $matches[2], $matches[3], $matches[1]);
        } else {
            $timestamp = strtotime($dateString);
            if ($timestamp) {
                return $timestamp;
            }
        }

        return null;
    }
}
