<?php

namespace common\models\xml;

use common\components\Util;


class BasicObject {

    protected $attributes = [];


    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        foreach ($data as $key => $value) {
            $key = Util::cleanXmlKey($key);
            $this->$key = $value;
        }
    }


    /**
     * @param string  $key
     * @param mixed   $value
     */
    public function set($key, $value)
    {
        $key = Util::cleanXmlKey($key);
        $this->$key = $value;
    }


    /**
     * @param  string  $key
     * @return mixed
     */
    public function get($key)
    {
        if (isset($this->$key) && $this->$key) {
            return $this->$key;
        }
        return null;
    }


    /**
     * @param array $data
     */
    public function setAttributes(array $data)
    {
        $this->attributes = [];
        foreach ($data as $key => $value) {
            if ($value) {
                $key = Util::cleanXmlKey($key);
                $this->attributes[$key] = $value;
            }
        }
    }


    /**
     * @param string  $key
     * @param mixed   $value
     */
    public function setAttribute($key, $value)
    {
        if ($value) {
            $key = Util::cleanXmlKey($key);
            $this->attributes[$key] = $value;
        }
    }


    /**
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        if (isset($this->attributes[$key]) && $this->attributes[$key]) {
            return $this->attributes[$key];
        }
        return null;
    }
}
