<?php

namespace common\models\xml;


class Property extends BasicObject {


    /**
     * @return string
     */
    public function getType()
    {
        return (isset($this->propertyType) ? $this->propertyType : '');
    }


    /**
     * @return int
     */
    public function getAgentId()
    {
        return (int) $this->get('agentID');
    }


    /**
     * @return int
     */
    public function getUniqueId()
    {
        return (int) $this->get('uniqueID');
    }


    /**
     * @param  bool  $asArray
     * @param  bool  $withLotNumber
     * @return string|array
     */
    public function getAddress($asArray = false, $withLotNumber = false)
    {
        $parts = [];

        if (isset($this->address)) {
            if ($withLotNumber && isset($this->address->lotNumber) && $this->address->lotNumber) {
                $parts['lot'] = $this->address->lotNumber;
            }

            if (isset($this->address->streetNumber) && $this->address->streetNumber) {
                $parts['streetNumber'] = $this->address->streetNumber;
            }

            if (isset($this->address->street) && $this->address->street) {
                $parts['street'] = $this->address->street . ($asArray ? '' : ',');
            }

            if (isset($this->address->suburb) && $this->address->suburb) {
                $parts['suburb'] = $this->address->suburb . ($asArray ? '' : ',');
            }

            if (isset($this->address->state) && $this->address->state) {
                $parts['state'] = $this->address->state . ($asArray ? '' : ',');
            }

            if (isset($this->address->postcode) && $this->address->postcode) {
                $parts['postcode'] = $this->address->postcode;
            }

            if ($asArray) {
                return $parts;
            } else {
                return implode(' ', $parts);
            }
        }

        return null;
    }


    /**
     * @return string
     */
    public function getAuthority()
    {
        $authority = $this->getAttribute('authority');
        if ($authority && isset($authority['value'])) {
            return $authority['value'];
        }

        return null;
    }


    /**
     * @return bool
     */
    public function getUnderOffer()
    {
        $underOffer = $this->getAttribute('underOffer');
        if ($underOffer && isset($underOffer['value']) && strtolower($underOffer['value']) == 'yes') {
            return true;
        }

        return false;
    }


    /**
     * @return string
     */
    public function getCategory()
    {
        $category = $this->getAttribute('category');
        if ($category && isset($category['name'])) {
            return $category['name'];
        }

        return null;
    }


    /**
     * @return string
     */
    public function getAuctionDate()
    {
        $auction = $this->getAttribute('auction');
        if ($auction && isset($auction['date'])) {
            return RenetProperties::parseDate($auction['date']);
        }

        return null;
    }


    /**
     * @return int
     */
    public function getBedrooms()
    {
        $features = $this->get('features');
        if (isset($features->bedrooms)) {
            return (int) $features->bedrooms;
        }

        return null;
    }


    /**
     * @return int
     */
    public function getBathrooms()
    {
        $features = $this->get('features');
        if (isset($features->bathrooms)) {
            return (int) $features->bathrooms;
        }

        return null;
    }


    /**
     * @return int
     */
    public function getGarages()
    {
        $features = $this->get('features');
        if (isset($features->garages)) {
            return (int) $features->garages;
        }

        return null;
    }


    /**
     * @return bool
     */
    public function isAuction()
    {
        if (strtolower($this->getCategory()) == 'auction') {
            return true;
        }

        return false;
    }


    /**
     * @return bool
     */
    public function isSold()
    {
        $soldDetails = $this->get('soldDetails');
        if ($soldDetails && isset($soldDetails->date) && $soldDetails->date) {
            return true;
        }

        return false;
    }


    /**
     * @return bool
     */
    public function getSoldPrice()
    {
        if ($this->isSold()) {
            $soldDetails = $this->get('soldDetails');
            if ($soldDetails && isset($soldDetails->price) && $soldDetails->price) {
                return $soldDetails->price;
            }
        }

        return null;
    }


    /**
     * @return string
     */
    public function getPriceView()
    {
        return $this->get('priceView');
    }


    /**
     * @return int
     */
    public function getSoldDate()
    {
        if ($this->isSold()) {
            $soldDetails = $this->get('soldDetails');
            if ($soldDetails && isset($soldDetails->date) && $soldDetails->date) {
                return RenetProperties::parseDate($soldDetails->date);
            }
        }

        return null;
    }


    /**
     * @return bool
     */
    public function showSoldPrice()
    {
        $soldDetails = $this->get('soldDetails');
        if ($soldDetails) {
            /* @var $soldDetails BasicObject */
            $priceAttr = $soldDetails->getAttribute('price');
            if ($priceAttr && isset($priceAttr['display']) && strtolower($priceAttr['display']) == 'yes') {
                return true;
            }
        }

        return false;
    }


    /**
     * @return string
     */
    public function getLandArea()
    {
        $landDetails = $this->get('landDetails');
        if ($landDetails) {
            /* @var $landDetails BasicObject */
            $areaAttr = $landDetails->getAttribute('area');
            $area     = $landDetails->get('area');

            if ($area && $areaAttr && isset($areaAttr['unit'])) {
                switch ($areaAttr['unit']) {
                    case 'squareMeter':  $unit = 'm2';               break;
                    default:             $unit = $areaAttr['unit'];  break;
                }

                return $area . $unit;
            }
        }

        return null;
    }


    /**
     * @return string
     */
    public function getBuildingArea()
    {
        $buildingDetails = $this->get('buildingDetails');
        if ($buildingDetails) {
            /* @var $buildingDetails BasicObject */
            $areaAttr = $buildingDetails->getAttribute('area');
            $area     = $buildingDetails->get('area');

            if ($area && $areaAttr && isset($areaAttr['unit'])) {
                switch ($areaAttr['unit']) {
                    case 'squareMeter':  $unit = 'm2';               break;
                    default:             $unit = $areaAttr['unit'];  break;
                }

                return $area . $unit;
            }
        }

        return null;
    }


    /**
     * @return array
     */
    public function getInspectionTimes()
    {
        $inspectionTimes = $this->get('inspectionTimes');
        if ($inspectionTimes && count($inspectionTimes)) {
            $result = [];

            foreach ($inspectionTimes as $inspectionTime) {
                /* @var $inspectionTime BasicObject */
                $result[] = $inspectionTime->get('value');
            }

            return $result;
        }

        return null;
    }


    /**
     * @return array
     */
    public function getImages()
    {
        $objects = $this->get('objects');
        if ($objects && count($objects)) {
            $result = [];

            foreach ($objects as $object) {
                /* @var $object BasicObject */
                if ($object->get('type') == 'img') {
                    $result[] = $object->get('url');
                }
            }

            return $result;
        }

        return null;
    }


    /**
     * @return array
     */
    public function getListingAgents()
    {
        $listingAgents = $this->get('listingAgent');

        if ($listingAgents && count($listingAgents)) {
            $agents = [];

            foreach ($listingAgents as $listingAgent) {
                $agent = [];
                foreach ($listingAgent as $item) {
                    if (is_a($item, 'common\models\xml\BasicObject')) {
                        /* @var $item BasicObject */
                        $name  = $item->get('name');
                        $value = $item->get('value');
                        $attr  = $item->attributes;
                        $type  = (isset($attr['type']) ? strtolower(trim($attr['type'])) : false);

                        if ($name == 'telephone') {
                            switch ($type) {
                                case 'mobile':
                                    $name = 'mobile';
                                    break;
                                default:
                                    $name = 'phone_' . $type;
                                    break;
                            }
                        }

                        $agent[$name] = $value;
                    }
                }
                $agents[] = $agent;
            }
            return $agents;
        }

        return null;
    }


    /**
     * @return bool
     */
    public function hasPool()
    {
        if (strtolower($this->get('pool')) == 'yes' || strtolower($this->get('poolInGround')) == 'yes') {
            return true;
        } else {
            $features = $this->get('features');
            if (isset($features->poolInGround) && strtolower($features->poolInGround) == 'yes') {
                return true;
            } else if (isset($features->poolAboveGround) && strtolower($features->poolAboveGround) == 'yes') {
                return true;
            }
        }

        return false;
    }


    /**
     * @return bool
     */
    public function hasAirCon()
    {
        if (strtolower($this->get('airConditioning')) == 'yes') {
            return true;
        }

        return false;
    }


    /**
     * @return bool
     */
    public function hasSplitSystemAirCon()
    {
        if (strtolower($this->get('splitsystemAircon')) == 'yes') {
            return true;
        }

        return false;
    }


    /**
     * @return bool
     */
    public function hasRumpusRoom()
    {
        if (strtolower($this->get('rumpusRoom')) == 'yes') {
            return true;
        }

        return false;
    }


    /**
     * @return bool
     */
    public function hasOutdoorEntertaining()
    {
        if (strtolower($this->get('outdoorEnt')) == 'yes') {
            return true;
        }

        return false;
    }


    /**
     * @return bool
     */
    public function hasAlarmSystem()
    {
        if (strtolower($this->get('alarmSystem')) == 'yes') {
            return true;
        }

        return false;
    }


    /**
     * @return bool
     */
    public function hasShed()
    {
        if (strtolower($this->get('shed')) == 'yes') {
            return true;
        }

        return false;
    }


    /**
     * @return bool
     */
    public function isFullyFenced()
    {
        if (strtolower($this->get('fullyFenced')) == 'yes') {
            return true;
        }

        return false;
    }


    /**
     * @return bool
     */
    public function hasDishwasher()
    {
        if (strtolower($this->get('dishwasher')) == 'yes') {
            return true;
        }

        return false;
    }


    /**
     * @return string
     */
    public function getHeadline()
    {
        return $this->get('headline');
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->get('description');
    }
}
