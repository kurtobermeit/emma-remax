/*jshint node:true */

'use strict';

var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var gulp = require('gulp');
var gutil = require('gulp-util');
var spawn = require('gulp-spawn');
var plumber = require('gulp-plumber');
var bourbon = require('node-bourbon');
var minifyCSS = require('gulp-minify-css');
var concat = require('gulp-concat-util');
var del = require('del');
var runSequence = require('run-sequence');


gutil.error  = logger('red', 'error');
gutil.warn   = logger('yellow', 'warn');
gutil.notice = logger('white', 'notice');


var scss = {
    frontend: {
        watch: 'frontend/scss',
        src:   'frontend/scss/styles.scss',
        dest:  'frontend/web/css'
    }
};


/**
 * Logger
 */
function logger(color, label) {
    return function () {
        var args = Array.prototype.slice.call(arguments);
        args.unshift(gutil.colors[color].bold(label.toUpperCase() + ':'));
        gutil.log.apply(null, args);
    };
}


/**
 * Handle Error
 */
function handleError(err) {
    gutil.error(err.stack);
}


function getSassCommand(data) {
    var includes = bourbon.with([data.watch]);
    return {
        cmd: path.resolve('./bin/sassc'),
        args: [
            '--stdin',
            //.'--style', 'compressed',
            '--sourcemap',
            '--load-path', includes.join(':')
        ],
        filename: function (base/*, ext*/) {
            return base + '.css';
        }
    };
}



/*****************************
 TASKS
******************************/


/**
 * css
 */
gulp.task('css', function ()
{
    gulp.src([scss.frontend.src])
        .pipe(plumber())
        .pipe(spawn(getSassCommand(scss.frontend)))
        .pipe(minifyCSS({
            keepSpecialComments: 0
        }))
        .pipe(concat.header('@import url(\'http://fonts.googleapis.com/css?family=Enriqueta:400,700\');'))
        .pipe(gulp.dest(path.resolve(scss.frontend.dest)))
        .on('error', handleError);
});


/**
 * fonts
 */
gulp.task('fonts', function()
{
    return gulp.src(['vendor/bower/font-awesome/fonts/*'])
        .pipe(gulp.dest(path.resolve('frontend/web/fonts')));
});


/**
 * clean
 */
gulp.task('clean', function (cb) {
    del(['./frontend/web/cdn'], cb);
});


/**
 * build
 */
gulp.task('build', function (cb)
{
    runSequence(['clean'],
        'fonts',
        ['css'],
        cb);
});



gulp.task('default', ['build'], function () {
    gulp.watch(scss.frontend.watch + '/**/*.scss', ['css']);
});
