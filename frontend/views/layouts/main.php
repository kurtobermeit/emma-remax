<?php
use frontend\assets\AppAsset;
use yii\helpers\Html;
use kartik\growl\Growl;

/* @var $this \yii\web\View */
/* @var $content string */

$user = &Yii::$app->controller->user;

AppAsset::register($this);

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= ($this->title ? Html::encode($this->title) . ' :: ' : '') . Html::encode(Yii::$app->params['siteName']) ?></title>
    <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="manifest" href="/manifest.json">
    <link rel="apple-touch-icon" sizes="57x57" href="/images/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/images/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>


<nav class="navbar navbar-default navbar-static-top" role="navigation">
    <section class="wrapper-xs bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    Email us at <i class="fa fa-envelope"></i> <a href="mailto:<?= Html::encode(Yii::$app->params['contactEmail']) ?>"><span class="text-light"><?= Html::encode(Yii::$app->params['contactEmail']) ?></span></a>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/images/logo.png" alt="Re/max">
            </a>
        </div>

        <div class="collapse navbar-collapse navbar-main-collapse">
            <ul class="nav navbar-nav">
                <li <?= ($this->context->id === 'site' ? 'class="active"' : '') ?>>
                    <a href="/">Home</a>
                </li>
                <li <?= ($this->context->id === 'sale' ? 'class="active"' : '') ?>>
                    <a href="/sale">Properties For Sale</a>
                </li>
                <li <?= ($this->context->id === 'sold' ? 'class="active"' : '') ?>>
                    <a href="/sold">Recently Sold</a>
                </li>
                <li <?= ($this->context->id === 'team' ? 'class="active"' : '') ?>>
                    <a href="/team">The Team</a>
                </li>
                <li <?= ($this->context->id === 'appraisal' ? 'class="active"' : '') ?>>
                    <a href="/appraisal">Request an Appraisal</a>
                </li>
                <li <?= ($this->context->id === 'review' ? 'class="active"' : '') ?>>
                    <a href="/review">Reviews</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<?= $content ?>

<footer class="footer-container">
    <section class="footer-primary">
        <div class="container">
            <div class="row">
                <!--
                <div class="col-sm-12 col-md-6">
                    <h3>Latest From Instagram</h3>
                    <iframe src="http://widget.websta.me/in/emma.de.marco/?r=1&w=5&h=2&b=0&p=5&sb=off" allowtransparency="true" frameborder="0" scrolling="no" style="border:none;overflow:hidden;width:525px; height: 210px" ></iframe>
                </div>-->
                <div class="col-md-6 col-lg-9">
                    <img class="logo" src="/images/logo.png" alt="Re/max">
                    <ul class="list-unstyled white-links">
                        <li><a href="/sale">Properties for Sale</a></li>
                        <li><a href="/sold">Recently Sold</a></li>
                        <li><a href="/team">The Team</a></li>
                        <li><a href="/appraisal">Request an Appraisal</a></li>
                        <li><a href="/review">Reviews</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h3>Contact Us</h3>
                    <ul class="list-unstyled">
                        <li>3/95 Mains Road</li>
                        <li>Sunnybank QLD 4109</li>
                        <li><i class="fa fa-angle-right"></i> <a href="mailto:<?= Html::encode(Yii::$app->params['contactEmail']) ?>"><?= Html::encode(Yii::$app->params['contactEmail']) ?></a></li>
                        <!--<li><i class="fa fa-angle-right"></i> <a href="javascript:void(null);">8:30am-6pm AEST Mon-Sat</a></li>-->
                        <li><i class="fa fa-angle-right"></i> P: &#40;&#48;&#55;&#41; &#51;&#51;&#52;&#51; &#52;&#51;&#53;&#51;</li>
                        <li><i class="fa fa-angle-right"></i> M: &#48;&#52;&#49;&#50; &#49;&#53;&#49; &#54;&#53;&#52;</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="footer-secondary">
        <div class="container">
            <div class="row">
                <div class="col-lg-10">
                    <p class="no-margin-bottom">&copy; <?= Html::encode(Yii::$app->params['ownerName']) ?> <?= date('Y') ?></p>
                </div>
                <div class="col-sm-2">
                    <a class="btn btn-facebook" href="https://www.facebook.com/TheRossBischoffTeam" target="_blank"><i class="fa fa-fw fa-facebook"></i></a>
                </div>
            </div>
        </div>
    </section>
</footer>

<?php
foreach (Yii::$app->session->getAllFlashes() as $message) {
    print Growl::widget([
        'type'          => (!empty($message['type'])) ? $message['type'] : 'danger',
        'title'         => (!empty($message['title'])) ? Html::encode($message['title']) : null,
        'icon'          => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
        'body'          => (!empty($message['message'])) ? Html::encode($message['message']) : 'Message Not Set!',
        'showSeparator' => true,
        'delay'         => 1, // This delay is how long before the message shows
        'pluginOptions' => [
            'delay'     => (!empty($message['duration'])) ? $message['duration'] : 4000, // This delay is how long the message shows for
            'placement' => [
                'from'  => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
            ]
        ],
        'options' => ['style' => 'font-size:13px']
    ]);
}
?>

<?php $this->endBody() ?>
</body>
</html>
<?php
$this->endPage();
