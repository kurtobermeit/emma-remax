<?php
/* @var $this yii\web\View */
/* @var $members common\models\Agent[] */

use yii\helpers\Html;
use common\components\Util;

$this->title = '';

?>


<section class="wrapper-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Meet our team</h1>
                <p class="lead">Every name has a story</p>
            </div>
        </div>
    </div>
</section>

<section class="wrapper-md bg-highlight">
    <div class="container">
        <?php
        foreach ($members as $agent) {
            ?>
            <div class="row">
                <div class="col-sm-6 col-md-3 col-md-offset-1">
                    <div class="thumbnail">
                        <div class="overlay-container">
                            <img src="/images/agent_<?= strtolower(str_replace(' ', '', $agent['name'])); ?>_profile.jpg">
                        </div>
                        <div class="thumbnail-meta">
                            <i class="fa fa-fw fa-briefcase"></i> Licensed Agent
                        </div>
                        <div class="thumbnail-meta">
                            <i class="fa fa-fw fa-envelope"></i> <a
                                href="mailto:<?= Html::encode($agent->email) ?>"><?= Html::encode($agent->email) ?></a>
                        </div>
                        <div class="thumbnail-meta">
                            <i class="fa fa-fw fa-phone"></i> <?= Html::encode($agent->phone) ?>
                        </div>
                        <div class="thumbnail-meta">
                            <a href="#link" class="btn btn-lg btn-block btn-success">Contact</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-7">
                    <h2><?= Html::encode($agent->name) ?> - Licensed Agent</h2>
                    <?php if ($agent->id === 1) { // EMMA ?>
                    <p>Emma De Marco has real estate in her blood, having witnessed her Father grow into an
                        award-winning agent. It’s no surprise that from an early age, Emma developed a strong
                        passion for real estate and the art of buying and selling as well as helping others to achieve
                        their property goals.</p>

                        <p>Emma purchased her first home at the ripe young age of 18 on Brisbane’s Southside. Emma
                        has experienced the ups and downs that come with property ownership; from renovations
                        to new builds and even the challenges of being a landlord. Emma has been successfully
                        buying properties and selling for a profit and attributes this to her natural ability to analyse
                        the market and make informed decisions. Emma is attuned to the emotions that come with
                        buying and selling property as she’s experienced them all in the 20 years spent building up
                        her property portfolio.</p>

                        <p>Emma has been a licensed real estate agent since 2002. Additionally, Emma has a Degree in
                        Business specialising in Management &amp; Human Resources.  Having spent many years in high
                        level Human Resources positions, Emma is invested in her clients’ individual circumstances
                        and goals. Emma is a strong believer in finding solutions for every situation and makes it
                        her priority to do so. Her extensive experience working with Ross Bischoff and his team, and
                        her training with industry experts has provided Emma with invaluable skills and knowledge.</p>

                        <p>Described as an utmost professional with strong negotiation abilities, Emma is renowned for
                        achieving record breaking results that are synonymous with the RE/MAX brand. Emma’s
                        clients are consistently rewarded by her strong attention to detail and determination to
                        produce exceptional results each and every time. Emma prides herself on her open and
                        honest communication skills and makes it a priority to keep her clients informed every step
                        of the way to ensure a comfortable and stress-free property journey.</p>

                        <p>Emma has lived locally for over 35 years and is an active member of the community. She has
                        a thorough, first hand understanding of the local market and the people who live there. She
                        is highly skilled at analysing market trends as they apply to the area and will share this
                        knowledge readily to benefit her clients.  Her passion, knowledge and energetic style make
                        Emma a rare commodity in the industry.</p>

                        <p>Perhaps most importantly, Emma is invested in the community. Partnering with and
                        supporting local businesses and sporting clubs is an integral part of Emma’s lifestyle. She is a
                        proud supporter of St Peter’s Catholic Primary School, Sunnybank Rugby Union Club,
                        Rochedale Rovers Soccer Club and Springwood Tigers Rugby League Club.</p>

                        <p>Outside of real estate, Emma loves nothing more than spending quality time with her sons.
                        Emma is a keen fitness enthusiast and a cappuccino connoisseur. She enjoys the local
                        lifestyle which surrounds her home in Eight Mile Plains.</p><br><br><br><br>
                    <?php } elseif ($agent->id === 2) { ?>
                        <p>Achieving outstanding results for his clients is something that clearly comes naturally to
                            Ross Bischoff. A multi-award winner including membership of the elite RE/MAX hall of fame
                            and with over 26 years of experience, Ross is among the top professionals in the real estate
                            industry.</p>

                        <p>A highly skilled negotiator and utmost professional, Ross shares the vision of delivering an
                            unparalleled level of service and achieving record-breaking results that is synonymous with
                            the RE/MAX brand. Ross&#39; clients are consistently rewarded by his energetic style, strategic
                            approach and unwavering determination, as well as his ability to listen to and respond to his
                            clients&#39; individual circumstances and goals.</p>

                        <p>Ross’ open and honest communication style ensures that he is able to form good
                            relationships quickly and that his clients are kept informed every step of the way. His
                            testimonials speak volumes about his clients&#39; appreciation of their experience with, and
                            results achieved by Ross, so it comes as no surprise that he has developed a loyal following
                            of delighted clients and referral business, many of whom have bought and sold with Ross
                            multiple times over the years.</p>

                        <p>Having lived locally for over 40 years, Ross has a thorough, first hand understanding of the
                            local market and the people who live there. For clients who are looking to buy or sell in the
                            area, his wealth of in-depth local knowledge and passion for the area are a genuine
                            advantage.</p>

                        <p>When he&#39;s not hard at work for his clients, Ross enjoys nothing more than being with his
                            family and enjoying the local lifestyle. His young grandchildren keep him on his toes and
                            enjoy an outing on the jet-ski as much as Ross does. A keen traveller, Ross has recently
                            returned from Spain and has Iceland earmarked for his next adventure.</p>

                        <ul>
                            <li>RE/MAX Member of Chairman’s Club 2003</li>
                            <li>RE/MAX Member of Platinum Club 2004</li>
                            <li>RE/MAX Member of Platinum Club 2005</li>
                            <li>RE/MAX Member of Chairman’s Club 2006</li>
                            <li>No 2 Sales Team Australia 2007</li>
                            <li>RE/MAX Member of Chairman’s Club 2008</li>
                            <li>Winner of Lifetime achiever award 2009</li>
                            <li>No 7 Sales Team International 2010</li>
                            <li>RE/MAX Member of Chairman’s Club 2010</li>
                            <li>RE/MAX Member of Platinum Club 2011</li>
                            <li>RE/MAX Member of Chairman’s Club 2012</li>
                            <li>RE/MAX Member of Chairman’s Club 2013</li>
                            <li>No 5 Sales Team Australia 2013</li>
                            <li>RE/MAX Member of Chairman’s Club 2014</li>
                            <li>No 8 Sales Team Australia 2014</li>
                            <li>RE/MAX Member of Chairman’s Club 2015</li>
                            <li>No 5 Sales Team Australia 2015</li>
                            <li>RE/MAX Member of Chairman’s Club 2016</li>
                            <li>No 8 Sales Team Australia 2016</li>
                        </ul>
                    <?php } ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</section>