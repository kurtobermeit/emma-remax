<?php
/* @var $this yii\web\View */
/* @var $properties common\models\Property[] */

use yii\helpers\Html;

$this->title = '';

?>


<section class="wrapper-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Properties For Sale</h1>
            </div>
        </div>
    </div>
</section>

<section class="wrapper-md bg-highlight">
    <div class="container">
        <div class="row">
            <?php
            foreach ($properties as $property) {
                $link = $property->getUrl();
                $photos = $property->photos;

                $photoUrl = '/images/item-small.jpg';

                if ($photos && count($photos)) {
                    $photo = array_shift($photos);
                    $photoUrl = $photo->url;
                }

                ?>
                <div class="col-sm-6 col-md-3">
                    <div class="property-thumb thumbnail">
                        <div class="overlay-container">
                            <div class="photo" style="background-image: url('<?= $photoUrl ?>');"></div>
                            <?php
                            if ($property->isSold) {
                                ?>
                                <img src="/images/sold.png" class="sash" />
                                <?php
                            } else if ($property->isUnderOffer) {
                                ?>
                                <img src="/images/under-contract.png" class="sash" />
                                <?php
                            }
                            ?>
                            <div class="overlay-content">
                                <h3 class="h4 headline"><a href="<?= $link ?>"><?= $property->headline ?></a></h3>
                                <?php /* <p>So you know you're getting a top quality property from an experienced team.</p> */ ?>
                            </div>
                        </div>
                        <div class="thumbnail-meta">
                            <p><i class="fa fa-fw fa-home"></i> <?= $property->getStreetAddress() ?></p>
                            <p><i class="fa fa-fw fa-map-marker"></i> <?= $property->getSuburbAddress() ?></p>
                        </div>
                        <div class="thumbnail-meta">
                            <strong><?= $property->bedrooms ?></strong> Bed | <strong><?= $property->bathrooms ?></strong> Bath | <strong><?= $property->garages ?></strong> Garage <?= ($property->landArea ? ' | ' . $property->landArea : '') ?>
                        </div>
                        <div class="thumbnail-meta">
                            <a href="<?= $link ?>" class="btn btn-link pull-right"><i class="fa fa-arrow-right"></i></a>
                            <a href="<?= $link ?>"><span class="h5"><?= $property->displayPrice ?></span></a>

                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</section>
