<?php
/* @var $this yii\web\View */
/* @var $property common\models\Property */

use yii\helpers\Html;
use common\components\Util;

$this->title = $property->getFullAddress();

$photos = $property->photos;

?>

<section class="wrapper-md bg-">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <article class="post">
                    <div id="my-carousel" class="carousel slide">
                        <ol class="carousel-indicators">
                            <?php
                            for ($x = 0; $x < count($photos); $x++) {
                                ?>
                                <li data-target="#my-carousel" data-slide-to="<?= $x ?>"<?= ($x === 0 ? ' class="active"' : '') ?>></li>
                                <?php
                            }
                            ?>
                        </ol>
                        <div class="carousel-inner">
                            <?php
                            $isFirstPhoto = true;
                            foreach ($photos as $photo) {
                                ?>
                                <div class="item<?= ($isFirstPhoto ? ' active' : '') ?>">
                                    <img class="img-responsive" src="<?= $photo->url ?>" alt="">
                                </div>
                                <?php
                                $isFirstPhoto = false;
                            }
                            ?>
                        </div>
                        <a class="left carousel-control" href="#my-carousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#my-carousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>

                        <?php
                        if ($property->isSold) {
                            ?>
                            <img src="/images/sold.png" class="sash" />
                            <?php
                        } else if ($property->isUnderOffer) {
                            ?>
                            <img src="/images/under-contract.png" class="sash" />
                            <?php
                        }
                        ?>

                    </div>

                    <h2><i class="fa fa-map-marker"></i> <?= Html::encode($property->getStreetAddress()) ?>, <span class="text-muted"><?= Html::encode($property->getSuburbAddress()) ?></span></h2>
                    <p class="lead"><?= Html::encode($property->headline) ?></p>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12 col-md-5">
                            <div class="widget padding-md bg-secondary">
                                <h3 class="headline">Features:</h3>
                                <ul class="list-unstyled">
                                    <?php
                                    if ($property->landArea) {
                                        ?>
                                        <li><i class="fa fa-fw fa-th"></i> <strong>Property:</strong> <?= Html::encode($property->landArea) ?></li>
                                        <?php
                                    }
                                    ?>
                                    <li><i class="fa fa-fw fa-columns"></i> <strong>Bedrooms:</strong> <?= Html::encode($property->bedrooms) ?></li>
                                    <li><i class="fa fa-fw fa-female"></i> <strong>Bathrooms:</strong> <?= Html::encode($property->bathrooms) ?></li>
                                    <li><i class="fa fa-fw fa-truck"></i> <strong>Garage:</strong> <?= Html::encode($property->garages) ?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-7">
                            <div class="panel panel-secondary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Description</h3>
                                </div>
                                <div class="panel-body">
                                    <p><?= nl2br(Html::encode(str_replace('â¢', '* ', $property->description))) ?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-secondary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Map location</h3>
                        </div>
                        <div class="panel-body padding-none">
                            <div class="embed-wrapper">
                                <iframe
                                    width="425"
                                    height="350"
                                    frameborder="0" style="border:0"
                                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDcHGg_xfuW5r5V7dkZ5i1nNswFMsiGUoA&q=<?= str_replace(' ', '+', Html::encode($property->getFullAddress())) ?>" allowfullscreen>
                                </iframe>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-secondary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Contact agent</h3>
                        </div>
                        <div class="panel-body padding-md">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <h3>Agent details</h3>

                                    <?php
                                    foreach ($property->listingAgents as $agent) {
                                        $avatar_uri = 'images/agent_' . strtolower(str_replace(' ', '', $agent['name'])) . '_profile_small.jpg';

                                        if (!file_exists($avatar_uri)) {
                                            $avatar_uri = 'images/empty-avatar.jpg';
                                        }
                                        ?>
                                        <div class="media">
                                            <a class="pull-left" href="#"><img class="media-object" src="/<?= $avatar_uri; ?>" width="64" height="64" alt="64x64"></a>
                                            <div class="media-body">
                                                <h4 class="media-heading"><?= Html::encode($agent['name']) ?></h4>
                                                <ul class="list-unstyled">
                                                    <?php
                                                    if (isset($agent['mobile'])) {
                                                        ?>
                                                        <li><i class="fa fa-fw fa-mobile"></i> <strong>Mobile:</strong> <?= Html::encode($agent['mobile']) ?></li>
                                                        <?php
                                                    }

                                                    if (isset($agent['phone_bh'])) {
                                                        ?>
                                                        <li><i class="fa fa-fw fa-phone"></i> <strong>Phone:</strong> <?= Html::encode($agent['phone_bh']) ?></li>
                                                        <?php
                                                    }

                                                    /*
                                                    if (isset($agent['email'])) {
                                                        ?>
                                                        <li><i class="fa fa-fw fa-envelope"></i> <strong>Email:</strong> <a href="mailto:<?= Html::encode($agent['email']) ?>"><?= Html::encode($agent['email']) ?></a></li>
                                                        <?php
                                                    }
                                                    */
                                                    ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <form role="form" method="post" action="/contact/appraisal">
                                        <div class="form-group">
                                            <label for="example-contact-name">Name</label>
                                            <input type="text" class="form-control" id="example-contact-name" placeholder="Your name" name="name" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-contact-email">Email address</label>
                                            <input type="email" class="form-control" id="example-contact-email" placeholder="Your email" name="email" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-contact-name">Phone Number</label>
                                            <input type="tel" class="form-control" id="example-contact-phone" placeholder="Your phone number" name="phone" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-contact-message">Message</label>
                                            <textarea id="example-contact-message" class="form-control" rows="5" name="message" minlength="10" required></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Send Message</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <!-- End of main -->

            <!-- Sidebar -->
            <div class="col-sm-12 col-md-3">
                <div class="panel panel-secondary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Inspection Times</h3>
                    </div>
                    <div class="panel-body">
                        <?php
                        if (count($property->inspectionTimes)) {
                            ?>
                            <ul>
                                <?php
                                foreach ($property->inspectionTimes as $key => $inspectionTime) {
                                    ?>
                                    <li><?= Html::encode($inspectionTime) ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <?php
                        } else {
                            ?>
                            <p>There are no inspection times planned. Please contact one of the agents below to arrange a time.</p>
                            <?php
                        }
                        ?>
                    </div>
                </div>

                <div class="panel panel-secondary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Agent Details</h3>
                    </div>
                    <div class="panel-body">
                        <?php
                        foreach ($property->listingAgents as $agent) {
                            $avatar_uri = 'images/agent_' . strtolower(str_replace(' ', '', $agent['name'])) . '_profile_small.jpg';

                            if (!file_exists($avatar_uri)) {
                                $avatar_uri = 'images/empty-avatar.jpg';
                            }
                            ?>
                            <div class="media">
                                <a class="pull-left" href="#"><img class="media-object" src="/<?= $avatar_uri; ?>" width="64" height="64" alt="64x64"></a>
                                <div class="media-body">
                                    <h4 class="media-heading"><?= Html::encode($agent['name']) ?></h4>
                                    <ul class="list-unstyled">
                                        <?php
                                        if (isset($agent['mobile'])) {
                                            ?>
                                            <li><i class="fa fa-fw fa-mobile"></i> <?= Html::encode($agent['mobile']) ?></li>
                                            <?php
                                        }

                                        if (isset($agent['phone_bh'])) {
                                            ?>
                                            <li><i class="fa fa-fw fa-phone"></i> <?= Html::encode($agent['phone_bh']) ?></li>
                                            <?php
                                        }

                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
