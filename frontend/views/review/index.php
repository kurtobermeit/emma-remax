<?php
/* @var $this yii\web\View */
/* @var $members common\models\Agent[] */

use yii\helpers\Html;
use common\components\Util;

$this->title = '';

?>


<section class="wrapper-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?= $agent->name ?>'s Reviews</h1>
                <p class="lead">See what our clients are saying.</p>
            </div>
        </div>
    </div>
</section>

<section class="wrapper-md bg-highlight">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3 col-md-offset-1">
                <div class="thumbnail">
                    <div class="overlay-container">
                        <img src="/images/agent_<?= strtolower(str_replace(' ', '', $agent['name'])); ?>_profile.jpg">
                    </div>
                    <div class="thumbnail-meta">
                        <i class="fa fa-fw fa-briefcase"></i> Licensed Agent
                    </div>
                    <div class="thumbnail-meta">
                        <i class="fa fa-fw fa-envelope"></i> <a
                            href="mailto:<?= Html::encode($agent->email) ?>"><?= Html::encode($agent->email) ?></a>
                    </div>
                    <div class="thumbnail-meta">
                        <i class="fa fa-fw fa-phone"></i> <?= Html::encode($agent->phone) ?>
                    </div>
                    <div class="thumbnail-meta">
                        <a href="#link" class="btn btn-lg btn-block btn-success">Contact</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-7 reviews">
                <?php if ($agent->id === 1) { // EMMA ?>
                    <h4>Gary & Kathy O <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains. </h5>
                    <p>Emma was very helpful and professional with our sale. She is a easy and pleasant to deal with and has extensive knowledge of the real estate market.</p>

                    <h4>Julie N <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Salisbury</h5>
                    <p>Emma De Marco sold our property within a week of it been on the market! Emma was delightful to deal with, her communication with us was of a high regard and she always remained professional. We couldn't have asked for a better real estate agent to help us with the sale. We will recommend her to any of our friends and family in the future. Thank you Emma for doing such a fantastic job!
                    </p>

                    <h4>Aaron S <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Macgregor</h5>
                    <p>Having my unit with other agents for the past two years then going to Re/Max, my unit was sold In four weeks. Re/Max is the place to go Emma De Marco is the agent to have. Emma is an amazing Real Estate agent that does more and takes care of the client. I highly recommend Emma De Marco to anyone who wants to sell.
                    </p>

                    <h4>Chris H <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Parkinson</h5>
                    <p>We had found Emma to be very personable and easy to talk too. Every communication felt easy and she explained the whole process of selling in a very easy to understand manner. I would recommend Emma as an agent for anyone wanting to sell or buy. Very lovely lady who works hard to get the result you want.
                    </p>

                    <h4>Brad & Helen F <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains</h5>
                    <p>Emma was a very easy person to communicate with and worked tirelessly to present our property to prospective buyers. We appreciate the time and effort she took in the selling of our home. We also feel that she was prepared to go the extra mile when it came to the positive presentation of our property for sale.
                    </p>

                    <h4>Merilyn A <span class="normal-weight">(Vendor)</span></h4>
                    <h5>Eight Mile Plains & Runcorn</h5>
                    <p>Emma kept me in the loop at all times regarding both the sale of my house in Eight Mile Plains and purchase of my new home in Runcorn. Nothing was too much trouble for her and her professional and personal aptitude was both outstanding. Emma explained everything in layman's terms which helped a lot.
                    </p>
                <?php } ?>
            </div>
        </div>
    </div>
</section>