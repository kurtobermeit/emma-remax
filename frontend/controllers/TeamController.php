<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use common\models\Agent;


/**
 * Team controller
 */
class TeamController extends Controller
{
    /**
     * Anonymous Actions allowed in this controller
     *
     * @var array
     */
    public $anonActions = ['index', 'emma', 'ross'];


    /**
     * /team
     *
     * @return string
     */
    public function actionIndex()
    {
        $teamEmails = $this->params['teamEmails'];
        $team = Agent::findAll(['email' => $teamEmails]);

        // Hack to custom order by the teamEmails array of members
        $members = [];
        foreach ($team as $member) {
            $index = array_search($member->email, $teamEmails);
            if ($index !== false) {
                $members[$index] = $member;
            }
        }

        return $this->render('index', [
            'members' => $members,
        ]);
    }
}
