<?php
namespace frontend\controllers;

use common\models\Property;
use Yii;
use frontend\components\Controller;
use common\models\Agent;


/**
 * Site controller
 */
class SoldController extends Controller
{
    /**
     * Anonymous Actions allowed in this controller
     *
     * @var array
     */
    public $anonActions = ['index', 'error'];


    /**
     * Site Homepage
     *
     * @return string
     */
    public function actionIndex()
    {
        $filterEmail = $this->params['filterAgentEmail'];
        if ($filterEmail) {
            $properties = Agent::findOne(['email' => $filterEmail])->getRecentSoldProperties(['createdTime' => SORT_DESC]);
        } else {
            $properties = Property::find()->where(['isSold' => 1])->orderBy(['createdTime' => SORT_DESC])->all();
        }

        return $this->render('index', [
            'properties' => $properties,
        ]);
    }


    /**
     * Error Page
     *
     * @return string
     */
    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        return $this->render('error', ['exception' => $exception]);
    }
}
