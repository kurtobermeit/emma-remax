<?php
namespace frontend\controllers;

use common\models\Property;
use Yii;
use frontend\components\Controller;
use common\models\Agent;


/**
 * Property controller
 */
class AppraisalController extends Controller
{
    /**
     * Anonymous Actions allowed in this controller
     *
     * @var array
     */
    public $anonActions = ['index'];
    public $enableCsrfValidation = false;

    /**
     * Site Homepage
     *
     * @return string
     */
    public function actionIndex()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $property = Property::findOne(['id' => 1]);
            $fromEmail = $this->request->post('email');
            $fromName  = $this->request->post('name');
            $fromPhone = $this->request->post('phone');
            $propertyAddress = $this->request->post('property_address');
            $suburb = $this->request->post('suburb');
            $postcode = $this->request->post('postcode');

            $toEmail   = Yii::$app->params['contactEmail'];
            $message   = $this->request->post('message');

            // Send email
            Yii::$app->mailer->compose([
                'html' => 'appraisal/html',
                'text' => 'appraisal/text',
            ], [
                'fromName' => $fromName,
                'fromEmail' => $fromEmail,
                'fromPhone' => $fromPhone,
                'propertyAddress' => $propertyAddress,
                'suburb' => $suburb,
                'postcode' => $postcode,
                'property' => $property,
                'msg' => $message
            ])
                ->setFrom($fromEmail)
                ->setTo($toEmail)
                ->setSubject('Appraisal Request from ' . $fromName)
                ->send();

            $this->notifySuccess('Thanks ' . $fromName . '! Your message has been sent. We\'ll get back to you soon.');
        }

        return $this->render('index');
    }
}
