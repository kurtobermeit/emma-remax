<?php
namespace frontend\controllers;

use Yii;
use frontend\components\Controller;
use common\models\Agent;


/**
 * Team controller
 */
class ReviewController extends Controller
{
    /**
     * Anonymous Actions allowed in this controller
     *
     * @var array
     */
    public $anonActions = ['index', 'emma', 'ross'];


    /**
     * /team
     *
     * @return string
     */
    public function actionIndex()
    {
        $teamEmails = $this->params['fromEmail'];
        $team = Agent::findOne(['email' => $teamEmails]);
//echo '<pre>'; print_r($team); die();
        return $this->render('index', [
            'agent' => $team,
        ]);
    }
}
