<?php
namespace frontend\controllers;

use common\models\Property;
use Yii;
use frontend\components\Controller;
use common\models\Agent;


/**
 * Property controller
 */
class PropertyController extends Controller
{
    /**
     * Anonymous Actions allowed in this controller
     *
     * @var array
     */
    public $anonActions = ['index'];


    /**
     * Site Homepage
     *
     * @return string
     */
    public function actionIndex()
    {
        $id = intval($this->request->getQueryParam('id'));
        if (!$id) {
            return $this->redirect('/');
        }

        $property = Property::findOne(['id' => $id]);
        if (!$property) {
            return $this->redirect('/');
        }

        // Make sure it's owned by the filter
        $filterEmail = $this->params['filterAgentEmail'];
        if ($filterEmail) {
            $agent = $property->getAgents()->where(['=', 'email', $filterEmail])
                ->limit(1)
                ->one();

            if (!$agent) {
                return $this->redirect('/');
            }
        }

        return $this->render('index', [
            'property' => $property,
        ]);
    }
}
