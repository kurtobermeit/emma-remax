<?php
namespace frontend\components;

use common\models\Language;
use Yii;
use yii\web\Controller as YiiController;
use common\models\User;


/**
 * Controller Component
 *
 * @property \yii\web\Session $session
 */
class Controller extends YiiController
{
    /**
     * @var User $user
     */
    public $user = null;

    /**
     * @var array $anonActions
     */
    public $anonActions = [];

    /**
     * @var array $params
     */
    public $params = [];

    /**
     * @var \yii\web\Request $request
     */
    public $request = null;

    /**
     * @var \yii\web\Session $session
     */
    public $session = null;

    /**
     * @var \yii\caching\ApcCache $cache
     */
    public $cache = null;



    /**
     * init
     *
     * @return void
     */
    public function init()
    {
        $this->params  = &Yii::$app->params;
        $this->request = &Yii::$app->request;
        $this->session = &Yii::$app->session;
        $this->cache   = &Yii::$app->cache;

        if ($this->session->isActive) {
            $this->session->open();
        }

        if ($this->loadUser()) {
            $this->user->lastSeenTime = time();
            $this->user->save();
        }
    }


    /**
     * @param \yii\base\Action $action
     * @param mixed $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        if ($this->session->isActive) {
            $this->session->close();
        }

        return parent::afterAction($action, $result);
    }


    /**
     * @param string $id
     * @param array $params
     * @return mixed|\yii\web\Response
     * @throws \yii\base\InvalidRouteException
     */
    public function runAction($id, $params = [])
    {
        // See if it's ok to view this controller->action anonymously
        $resolvedActionName = ($id ? $id : $this->defaultAction);

        if (!$this->user && !in_array($resolvedActionName, $this->anonActions)) {
            // nope
            $this->session->set('redirectAfterLogin', $_SERVER['REQUEST_URI']);
            return $this->redirect('/login');
        } else {
            return parent::runAction($id, $params);
        }
    }


    /**
     * loadUser
     *
     * @return bool
     */
    protected function loadUser()
    {
        // See if session has user
        if ($this->session->get('userId') && $this->session->get('userKey')) {
            // Try to load from Session
            $userId  = (int) $this->session->get('userId');
            $userKey = $this->session->get('userKey');
            $user    = User::findOne($userId);

            if ($user && $user->validateUserKey($userKey) && $user->isActive) {
                // OK
                $this->user = $user;
                return true;
            }
        }

        // See if we need to load from Cookie
        if (isset($_COOKIE['k'])) {
            $cookieKey = $_COOKIE['k'];
            $user    = User::loadFromCookieKey($cookieKey);

            if ($user && $user->isActive) {
                $this->user = $user;
                return true;
            }
        }

        return false;
    }


    /**
     * Add a Success Flash Notification
     *
     * @param string  $message
     * @param string  $fontawesomeIcon
     */
    public function notifySuccess($message, $fontawesomeIcon = 'fa-check')
    {
        $this->session->setFlash('success', [
            'type'     => 'success',
            'icon'     => 'fa ' . $fontawesomeIcon,
            'message'  => $message,
        ]);
    }


    /**
     * Add a Error Flash Notification
     *
     * @param string  $message
     * @param string  $fontawesomeIcon
     */
    public function notifyError($message, $fontawesomeIcon = 'fa-times')
    {
        $this->session->setFlash('error', [
            'type'     => 'danger',
            'duration' => 10000,
            'icon'     => 'fa ' . $fontawesomeIcon,
            'message'  => $message,
        ]);
    }
}
