$(document).ready(function() {
    // Use lazy loading to prevent bottom images until after the main background image has loaded.
    // The init function has a setTimeout so we wait 5 seconds before loading.
    $('.photo').each(function(index, element) {
        $(element).lazyload({
            event: 'loaded'
        });
    });
});

var APP = {

    //
    // APP.Init
    //
    Init: {
        initialisers: [],
        unloaders: [],

        add: function (add_fn) {
            APP.Init.initialisers.push(add_fn);
        },

        addUnloader: function (add_fn) {
            APP.Init.unloaders.push(add_fn);
        },

        remove: function (remove_fn) {
            var last_init = APP.Init.initialisers.length - 1;

            for (var i = last_init; i >= 0; i--) {
                if (APP.Init.initialisers[i] === remove_fn) {
                    APP.Init.initialisers[i] = null;
                }
            }
        },

        removeUnloader: function (remove_fn) {
            var last_unloader = APP.Init.unloaders.length - 1;

            for (var i = last_unloader; i >= 0; i--) {
                if (APP.Init.unloaders[i] === remove_fn) {
                    APP.Init.unloaders[i] = null;
                }
            }
        },

        run: function () {
            var last_init = APP.Init.initialisers.length - 1;

            for (var i = 0; i <= last_init; ++i) {
                if (typeof(APP.Init.initialisers[i]) == 'function') {
                    APP.Init.initialisers[i]();
                }
            }
        },

        runUnload: function () {
            var last_unloader = APP.Init.unloaders.length - 1;

            for (var i = 0; i <= last_unloader; i++) {
                if (typeof(APP.Init.unloaders[i]) == 'function') {
                    APP.Init.unloaders[i]();
                }
            }
        }
    },


    //
    // APP.Log
    //
    Log: function (item) {
        if (typeof console != 'undefined' && console && typeof console.log != 'undefined' && console.log) {
            console.log(item);
        }
    },


    //
    // APP.Fn
    //
    Fn: {

        stop: function (e) {
            if (e) {
                e.preventDefault();
                e.stopPropagation();
            } else if (typeof(event) == 'object') {
                e = event;
                e.cancelBubble = true;
            }
            return e;
        },

        rndString: function (slength) {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz";
            if (!slength || typeof(slength) != 'number') {
                slength = 8;
            }
            var rs = '';
            for (var i = 0; i < slength; i++) {
                var rnum = Math.floor(Math.random() * chars.length);
                rs += chars.substring(rnum, rnum + 1);
            }
            return rs;
        }

    },


    //
    // APP.XHR
    //
    XHR: {
        last_err: false,
        last_err_code: false,
        err_should_notify: true,
        allow_xhr_requests: true,
        ignore_parse_error: false,
        ignore_all_errors: false,

        request: function (xhr_file, method, params, success_fn, failure_fn, loading_fn, complete_fn, ignore_parse_error, ignore_all_errors) {
            params = params || {};
            method = method || 'GET';

            APP.XHR.ignore_parse_error = (ignore_parse_error ? true : false);
            APP.XHR.ignore_all_errors = (ignore_all_errors ? true : false);

            if (APP.XHR.allow_xhr_requests) {
                if (method.toUpperCase() == 'GET') {
                    params.isxhr = '1';
                }

                $.ajax({
                    url: xhr_file + (method.toUpperCase() == 'POST' ? '?isxhr=1' : ''),
                    global: false,
                    type: method,
                    data: params,
                    dataType: 'json',
                    cache: false,
                    beforeSend: function (xhr) {
                        if (loading_fn) {
                            loading_fn();
                        }
                        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                        return xhr;
                    },

                    success: function (json) {
                        if (APP.XHR.isJsonError(json)) {
                            if (APP.XHR.err_should_notify) {
                                if (failure_fn) {
                                    failure_fn(APP.XHR.last_err, true);
                                } else {
                                    APP.XHR.notifyError();
                                }
                            }
                        } else if (APP.XHR.checkJsonRedirect(json)) {
                            // aldready taken care of.
                        } else if (success_fn) {
                            success_fn(json);
                        }
                    },

                    error: function (xhr, status) {
                        if (APP.XHR.ignore_all_errors || (status == 'parsererror' && APP.XHR.ignore_parse_error)) {
                            return;
                        }
                        if (failure_fn) {
                            failure_fn(status);
                        } else {
                            APP.XHR.notifyError('An error has occured', status);
                        }
                    },

                    complete: function () {
                        if (complete_fn) {
                            complete_fn();
                        }

                    }
                });
            }
        },

        isJsonError: function (json) {
            if (typeof(json) == 'undefined' || json == null) {
                APP.XHR.last_err = 'No result returned';
                APP.XHR.last_err_code = -1;
                return true;
            } else if (json.error && json.error_msg) {
                APP.XHR.last_err = json.error_msg;
                APP.XHR.last_err_code = json.error;
                return true;
            }

            return false;
        },

        checkJsonRedirect: function (json) {
            if (json && json.is_redirect && json.url) {
                window.location = json.url;
                return true;
            }
            return false;
        },

        notifyError: function (err_msg, status) {
            if (!err_msg) {
                err_msg = APP.XHR.last_err;
            }
            alert(err_msg);
        },

        allowXhrRequests: function (allow_them) {
            APP.XHR.allow_xhr_requests = (allow_them === true);
        }
    },

    Remax: {

        init: function () {
            // Once page has loaded, we trigger the rest of images to load. If we don't do this, the bottom images load before the background and it looks ugly
            $('.photo').each(function(index, element) {
                $(element).trigger('loaded');
            });
        }
    }
};


APP.Init.add(APP.Remax.init);


/* Initialise if prerequisites are available; */
if (typeof(jQuery) != 'undefined') {
    $(APP.Init.run);
    $(window).unload(APP.Init.runUnload);
} else {
    APP = false;
}
